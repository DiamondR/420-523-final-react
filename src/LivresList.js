import React, { Component } from 'react';
import LivresListHeader from './LivresListHeader';
import LivresListItem from './LivresListItem';
import _ from 'lodash';
import './App.css';

export default class LivresList extends Component {

  renderItems() {
    const props = _.omit(this.props, 'livres');
    return _.map(this.props.livres, livre => <LivresListItem key={livre.id} {...livre} {...props} />);
  }

  render() {
    return (
      <table>
        <LivresListHeader/>
        <tbody>
          {this.renderItems()}
        </tbody>
      </table>
    );
  }
}
